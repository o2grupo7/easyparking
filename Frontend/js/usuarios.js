
function loadData() {
    let request = sendRequest('usuario/list', 'GET', "");
    let table = document.getElementById('users-table');
    table.innerHTML = "";
    request.onload = function () {
        let data = request.response;
        data.forEach(element => {
            table.innerHTML += `
            <tr>
                <th>${element.idUsuario}</th>
                <td>${element.usuario}</td>
                <td>${element.clave}</td>
                <td>
                    <div class="form-check form-swich">
                        <input ${element, activo ? "checked" : "unchecked"}
                        class="form-check-input" type="checkbox" role="switch" disabled>
                    </div>
                </td>
                <td>
                    <button type="button" class="btn btn-primary"
                    onclick="window.location = "/form_usuarios.html?id=${element.idUsuario}"'>Editar</button>
                    <button type = "button" class="btn btn-danger"
                    onclick = 'deleteUser(${element.idUsuario})'>Eliminar<button>
                </td >
            <tr>
            `
        });
    }
    request.onerror = function() {
        table.innerHTML = `
        <tr>
            <td colspan="5">Error al recuperar los datos.</td> 
        </tr>
        `;
    }
}

// Para los botones de acción de Editar y Eliminar definiremos los valores a su atributo onclick:
// ● Para el atributo onclick de Editar especificamos a donde lo redireccionaremos además de proporcionarle un 
// parámetro id del usuario a la URL, el cual nos servirá para recuperar los datos a la hora de editar nuestro 
// usuario.
// ● Para el atributo onclick de Eliminar especificamos la función deleteUser() que recibe un parámetro idUsuario y 
// la definiremos a continuación.
// A continuación implementaremos la función deleteUser() que recibirá un parámetro idUsuario que nos servirá para 
// concatenarlo al endpoint. Una vez finalizada la petición llamaremos nuevamente a la función loadData() para 
// cargar los datos en nuestra tabla

function deleteUser(idUsuario) {
    let request = sendRequest('usuario/' + idUsuario, 'DELETE', '');
    request.onload = function () {
        loadData()
    }
}

// En la función onload obtenemos la respuesta de nuestra petición que nos llegará como un objeto desde nuestra 
// API y posteriormente accederemos a las propiedades haciendo el uso del punto (.) seguido del nombre de la 
// propiedad como en nuestro ejemplo el objeto está representado por la variable data sería data.propiedad.
// Del objeto recibido en la petición le asignaremos a nuestros elementos del formulario el valor correspondiente a 
// cada uno de ellos.

function loadUser(idUsuario) {
    let request = sendRequest('usuario/list' + idUsuario, 'GET', '');
    let id = document.getElementById("user-id")
    let email = document.getElementsById("user-email")
    let password = document.getElementsById("user-password")
    let status = document.getElementById("user-status")
    request.onload = function () {

        let data = request.response;
        id.value = data.idUsuario
        email.value = data.usuario
        password.value = data.clave
        status.checked = data.activo

    }
    request.onerror = function () {
        alert("Error al recuperar los datos.");
    }
}


// A continuación implementaremos la función saveUser(). 
// Esta función la implementaremos para editar(Solo si existe el valor id del elemento user-id) y registrar usuarios. 
// Una vez obtenido los valores de cada elemento del formulario pasaremos a crear un objeto con las propiedades y 
// sus respectivos valores que será proporcionado a la petición. Una vez finalizada la petición redireccionamos a 
// usuarios.html

function saveUser() {
    let email = document.getElementById('user-email').value
    let password = document.getElementById('user-password').value
    let id = document.getElementById('user-id').value
    let status = document.getElementById('user-status').value
    let data = { 'idUsuario': id, 'usuario': email, 'clave': password, 'activo': status }
    let request = sendRequest('usuario/', id ? 'PUT' : 'POST', data)
    request.onload = function () {
        window.location = 'usuarios.html';
    }
    request.onerror = function () {
        alert("Error al guardar los cambios")
    }

}