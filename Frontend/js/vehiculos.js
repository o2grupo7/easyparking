// En la función loadData() haremos uso de la función sendRequest() creada anteriormente en request.js donde le 
// proporcionamos el endpoint, el método y la información para esta petición será un string vacío.
// ● Obtenemos nuestro elemento table desde el HTML haciendo uso de document.getElementById() y eliminamos 
// los elementos hijos con la propiedad innerHTML.
// ● Implementaremos las funciones onload y onerror para obtener los resultados. En la función onerror definimos 
// una fila de nuestra tabla que mostrará un mensaje indicando que hubo un error durante la petición.
// ● En la función onload obtenemos la respuesta de nuestra petición que nos llegará como un array de objetos 
// desde nuestra API y posteriormente la recorremos mediante la función forEach de los arrays, para cada 
// elemento de nuestro array agregaremos una nueva fila a la tabla con la información del producto que está 
// representado como un objeto, para acceder a las propiedades hacemos uso del punto (.) seguido del nombre 
// de la propiedad como en nuestro ejemplo el objeto está representado por el element del ciclo forEach sería 
// element.propiedad.
// Para los botones de acción de Editar y Eliminar definiremos los valores a su atributo onclick:
// ● Para el atributo onclick de Editar especificamos a donde lo redireccionaremos además de proporcionarle un 
// parámetro id del usuario a la URL, el cual nos servirá para recuperar los datos a la hora de editar nuestro 
// producto.
// ● Para el atributo onclick de Eliminar especificamos la función deleteProducto() que recibe un parámetro 
// idProducto y la definiremos a continuación.
function loadData() {
    let request = sendRequest('vehiculo/list', 'GET', "");
    let tabla = document.getElementById('vehiculo-table');
    table.innerHTML = "";
    request.onload = function () {

        let data = request.response;
        data.forEach(element => {
            table, innerHTML += `
            <tr>
                <th>${element.idVehiculo}</th>
                <td>${element.tipoVehiculo}</td>
                <td>${element.placa}</td>
                <td>${element.celular}</td>
                <td>${element.observacion}</td>
                <td>${element.fechaIngreso}</td>
                <td>${element.fechaSalida}</td>
                <td>${element.horaIngreso}</td>
                <td>${element.horaSalida}</td>
                <td>${element.tipoPlaza}</td>
                <td>${element.estadoPlaza}</td>
                <td>
                    <button type="button" class="btn btn-primary"
                    onclick='window.location = "/form_vehiculos.html?id=${element.idVehiculo}"'>Editar</button>
                    <button type="button" class="btn btn-danger"
                    onclick='deleteVehiculo(${element.idVehiculo})'>Eliminar<button>
                </td>
            <tr>
            `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
        <tr>
            <td colspan="6"> al recuperar los datos.</td>
            </tr>
        `;    
    }
}

// A continuación implementaremos la función deleteProducto() que recibirá un parámetro idProducto que nos servirá 
// para concatenarlo al endpoint. Una vez finalizada la petición llamaremos nuevamente a la función loadData() para 
// cargar los datos en nuestra tabla.

function deleteVehiculo(idVehiculo) {
    let request = sendRequest('vehiculo/'+idVehiculo, 'DELETE', '');
    request.onload = function() {
        loadData()
    }
}

// En la función loadVehiculo() haremos uso de la función sendRequest() creada anteriormente en request.js donde 
// le proporcionamos el endpoint, el método y la información para esta petición será un string vacío.
// ● Obtenemos nuestros elementos del formulario desde el HTML haciendo uso de document.getElementById().
// ● Implementaremos las funciones onload y onerror para obtener los resultados. En la función onerror definimos 
// una alerta que mostrará un mensaje indicando que hubo un error durante la petición.
// ● En la función onload obtenemos la respuesta de nuestra petición que nos llegará como un objeto desde 
// nuestra API y posteriormente accederemos a las propiedades haciendo el uso del punto (.) seguido del nombre 
// de la propiedad como en nuestro ejemplo el objeto está representado por la variable data sería 
// data.propiedad
// Del objeto recibido en la petición le asignaremos a nuestros elementos del formulario el valor correspondiente a 
// cada uno de ellos.

function loadVehiculo() {
    let request = sendRequest('vehiculo/list' + idVehiculo, 'GET', '');
    let id = document.getElementsById("idVehiculo").value;
    let tipoVehiculo = document.getElementsById("tipoVehiculo").value;
    let placa = document.getElementById("placa").value;
    let celular = document.getElementById("celular").value;
    let fechaIngreso = document.getElementById("fechaIngreso").value;
    let fechaSalida = document.getElementById("fechaSalida").value;
    let horaIngreso = document.getElementById("horaIngreso").value;
    let horaSalida = document.getElementById("horaSalida").value;
    let observacion = document.getElementById("observacion").value;
    let tipoPlaza = document.getElementById("tipoPlaza").value;
    let estadoPlaza = document.getElementById("estadoPlaza").value;
    request.onload = function () {

        let data = request.response
        id.value = data.idVehiculo
        tipoVehiculo.value = data.tipoVehiculo
        placa.value = data.placa
        celular.value = data.celular
        fechaIngreso.value = data.fechaIngreso
        fechaSalida.value = data.fechaSalida
        horaIngreso.value = data.horaIngreso
        horaSalida.value = data.horaSalida
        observacion.value = data.observacion
        tipoPlaza.value = data.tipoPlaza
        estadoPlaza.value = data.estadoPlaza
    }
    request.onerror = function () {
        alert("Error al recuperar los datos");
    }
}
// Esta función la implementaremos para editar(Solo si existe el valor id del elemento product-id) y registrar 
// productos. Una vez obtenido los valores de cada elemento del formulario pasaremos a crear un objeto con las 
// propiedades y sus respectivos valores que será proporcionado a la petición. Una vez finalizada la petición 
// redireccionamos a productos.html
function saveVehiculo() {
    let id = document.getElementsById("idVehiculo").value;
    let tipoVehiculo = document.getElementsById("tipoVehiculo").value;
    let placa = document.getElementById("placa").value;
    let celular = document.getElementById("celular").value;
    let fechaIngreso = document.getElementById("fechaIngreso").value;
    let fechaSalida = document.getElementById("fechaSalida").value;
    let horaIngreso = document.getElementById("horaIngreso").value;
    let horaSalida = document.getElementById("horaSalida").value;
    let observacion = document.getElementById("observacion").value;
    let tipoPlaza = document.getElementById("tipoPlaza").value;
    let estadoPlaza = document.getElementById("estadoPlaza").value;
    let data = {
        'idVehiculo': id, 'tipoVehiculo': tipoVehiculo, 'placa': placa,
        'celular': celular, 'fechaIngreso': fechaIngreso, 'fechaSalida': fechaSalida,
        'horaIngreso': horaIngreso, 'horaSalida': horaSalida,
        'observacion': observacion, 'tipoPlaza': tipoPlaza,
        'estadoPlaza': estadoPlaza
    }
    let request = sendRequest('vehiculo/', id ? 'PUT' : 'POST', data);
    request.onload = function () {
        window.location = 'vehiculos.html';
    }
    request.onerror = function () {
        alert('Error al guardar los cambios.')
    }
}




