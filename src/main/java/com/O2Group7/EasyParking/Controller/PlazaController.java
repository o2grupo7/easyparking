package com.O2Group7.EasyParking.Controller;

import com.O2Group7.EasyParking.model.Plaza;
import com.O2Group7.EasyParking.model.Vehiculo;
import com.O2Group7.EasyParking.model.Tarifa;
import com.O2Group7.EasyParking.service.PlazaService;
import com.O2Group7.EasyParking.service.VehiculoService;
import com.O2Group7.EasyParking.service.TarifaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Plaza")
public class PlazaController {
    @Autowired
    private PlazaService plazaservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Plaza> agregar(@RequestBody Plaza plaza){
    Plaza obj = plazaservice.save(plaza);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Plaza> eliminar(@PathVariable Integer id){
    Plaza obj = plazaservice.findById(id);
    if(obj!=null)
    plazaservice.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Plaza> editar(@RequestBody Plaza plaza){
    Plaza obj = plazaservice.findById(plaza.getIdPlaza());
    if(obj!=null)
        {
        obj.setTipoPlaza(plaza.getTipoPlaza());
        obj.setEstadoPlaza(plaza.getEstadoPlaza());        
    plazaservice.save(obj);
    }
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Plaza> consultarTodo(){
    return plazaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Plaza consultaPorId(@PathVariable Integer id){
    return plazaservice.findById(id);
    }

}
