package com.O2Group7.EasyParking.Controller;

import com.O2Group7.EasyParking.model.Transaccion;
import com.O2Group7.EasyParking.model.Vehiculo;
import com.O2Group7.EasyParking.model.Tarifa;
import com.O2Group7.EasyParking.service.TransaccionService;
import com.O2Group7.EasyParking.service.VehiculoService;
import com.O2Group7.EasyParking.service.TarifaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Transaccion")
public class TransaccionController {
    
    @Autowired
    private TransaccionService transaccionservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Transaccion> agregar(@RequestBody Transaccion transaccion){
    Transaccion obj = transaccionservice.save(transaccion);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Transaccion> eliminar(@PathVariable Integer id){
    Transaccion obj = transaccionservice.findById(id);
    if(obj!=null)
        transaccionservice.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Transaccion> editar(@RequestBody Transaccion transaccion){
    Transaccion obj = transaccionservice.findById(transaccion.getIdTransaccion());
    if(obj!=null)
    {
        obj.setIdPlaza(transaccion.getIdPlaza());
        obj.setIdVehiculo(transaccion.getIdVehiculo());
        obj.setIdTarifa(transaccion.getIdTarifa());
        obj.setIdTarifa(transaccion.getIdUsuario());
        obj.setFechaIngreso(transaccion.getFechaIngreso());
        obj.setFechaSalida(transaccion.getFechaSalida());
        obj.setHoraIngreso(transaccion.getHoraIngreso());
        obj.setHoraSalida(transaccion.getHoraSalida());
        obj.setTotal(transaccion.getTotal());        
    transaccionservice.save(obj);
    }
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Transaccion> consultarTodo(){
    return transaccionservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Transaccion consultaPorId(@PathVariable Integer id){
    return transaccionservice.findById(id);
    }
    
}
