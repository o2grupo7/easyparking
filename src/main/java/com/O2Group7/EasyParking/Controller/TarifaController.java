package com.O2Group7.EasyParking.Controller;

import com.O2Group7.EasyParking.model.Tarifa;
import com.O2Group7.EasyParking.service.TarifaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Tarifa")
public class TarifaController {
    
    @Autowired
    private TarifaService tarifaservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Tarifa> agregar(@RequestBody Tarifa tarifa){
    Tarifa obj = tarifaservice.save(tarifa);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Tarifa> eliminar(@PathVariable Integer id){
    Tarifa obj = tarifaservice.findById(id);
    if(obj!=null)
        tarifaservice.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Tarifa> editar(@RequestBody Tarifa tarifa){
    Tarifa obj = tarifaservice.findById(tarifa.getIdTarifa());
    if(obj!=null)
    {
        obj.setTipoTarifa(tarifa.getTipoTarifa());
        obj.setTarifaPlaza(tarifa.getTarifaPlaza());
        obj.setValor(tarifa.getValor());
        obj.setDescripcion(tarifa.getDescripcion());
    tarifaservice.save(obj);
    }
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Tarifa> consultarTodo(){
    return tarifaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Tarifa consultaPorId(@PathVariable Integer id){
    return tarifaservice.findById(id);
    }
}
