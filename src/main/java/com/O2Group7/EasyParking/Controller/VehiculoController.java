package com.O2Group7.EasyParking.Controller;

import com.O2Group7.EasyParking.model.Vehiculo;
import com.O2Group7.EasyParking.service.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Vehiculo")
public class VehiculoController {
    @Autowired
    private VehiculoService vehiculoservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Vehiculo> agregar(@RequestBody Vehiculo vehiculo){
    Vehiculo obj = vehiculoservice.save(vehiculo);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Vehiculo> eliminar(@PathVariable Integer id){
    Vehiculo obj = vehiculoservice.findById(id);
    if(obj!=null)
        vehiculoservice.delete(id);
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Vehiculo> editar(@RequestBody Vehiculo vehiculo){
    Vehiculo obj = vehiculoservice.findById(vehiculo.getIdVehiculo());
    if(obj!=null)
        {
        obj.setTipoVehiculo(vehiculo.getTipoVehiculo());
        obj.setPlaca(vehiculo.getPlaca());   
        obj.setCelular(vehiculo.getCelular());
        obj.setObservacion(vehiculo.getObservacion());        
    vehiculoservice.save(obj);
    }
    else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Vehiculo> consultarTodo(){
    return vehiculoservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Vehiculo consultaPorId(@PathVariable Integer id){
    return vehiculoservice.findById(id);
    }
   
    
}
