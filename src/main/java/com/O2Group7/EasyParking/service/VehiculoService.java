/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Group7.EasyParking.service;

import com.O2Group7.EasyParking.model.Vehiculo;
import java.util.List;


/**
 *
 * @author norma
 */
public interface VehiculoService {
    public Vehiculo save(Vehiculo vehiculo);
    public void delete(Integer id);
    public Vehiculo findById(Integer id);
    public List<Vehiculo> findAll();
}
