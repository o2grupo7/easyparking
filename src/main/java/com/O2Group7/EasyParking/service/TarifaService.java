/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Group7.EasyParking.service;

import com.O2Group7.EasyParking.model.Tarifa;
import java.util.List;

/**
 *
 * @author norma
 */
public interface TarifaService {
    public Tarifa save(Tarifa tarifa);
    public void delete(Integer id);
    public Tarifa findById(Integer id);
    public List<Tarifa> findAll();
}
