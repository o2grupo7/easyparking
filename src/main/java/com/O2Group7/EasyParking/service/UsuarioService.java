/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Group7.EasyParking.service;

import com.O2Group7.EasyParking.model.Usuario;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public interface UsuarioService {
public Usuario save(Usuario usuario);
public void delete(Integer id);
public Usuario findById(Integer id);
public List<Usuario> findAll();

    public Usuario login(String usuario, String clave);


}

