/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Group7.EasyParking.service.implement;

import com.O2Group7.EasyParking.dao.TarifaDao;
import com.O2Group7.EasyParking.model.Tarifa;
import com.O2Group7.EasyParking.service.TarifaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author norma
 */
@Service
public class TarifaServiceImpl implements TarifaService {

    @Autowired
    private TarifaDao tarifaDao;

    @Override
    @Transactional(readOnly = false)
    public Tarifa save(Tarifa tarifa) {
        return tarifaDao.save(tarifa);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        tarifaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Tarifa findById(Integer id) {
        return tarifaDao.findById(id).orElse(null);
    }
    @Override
    @Transactional(readOnly = true)
    public List<Tarifa> findAll() {
        return (List<Tarifa>) tarifaDao.findAll();
    }
}
