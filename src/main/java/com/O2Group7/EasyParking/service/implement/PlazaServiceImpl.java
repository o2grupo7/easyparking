/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Group7.EasyParking.service.implement;

import com.O2Group7.EasyParking.dao.PlazaDao;
import com.O2Group7.EasyParking.model.Plaza;
import com.O2Group7.EasyParking.service.PlazaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author norma
 */

@Service
public class PlazaServiceImpl implements PlazaService {

    @Autowired
    private PlazaDao plazaDao;

    @Override
    @Transactional(readOnly = false)
    public Plaza save(Plaza plaza) {
        return plazaDao.save(plaza);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        plazaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Plaza findById(Integer id) {
        return plazaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Plaza> findAll() {
        return (List<Plaza>) plazaDao.findAll();
    }
}
