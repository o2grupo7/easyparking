/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Group7.EasyParking.service;


import com.O2Group7.EasyParking.model.Plaza;
import java.util.List;

/**
 *
 * @author norma
 */

public interface PlazaService {
    public Plaza save(Plaza plaza);
    public void delete(Integer id);
    public Plaza findById(Integer id);
    public List<Plaza> findAll();
}