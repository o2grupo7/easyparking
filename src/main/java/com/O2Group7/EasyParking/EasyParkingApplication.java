package com.O2Group7.EasyParking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyParkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyParkingApplication.class, args);
	}

}
