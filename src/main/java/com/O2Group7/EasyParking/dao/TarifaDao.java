/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O2Group7.EasyParking.dao;

import com.O2Group7.EasyParking.model.Tarifa;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author USUARIO
 */
public interface TarifaDao extends CrudRepository<Tarifa, Integer> {
    
}
