/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O2Group7.EasyParking.dao;

import com.O2Group7.EasyParking.model.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author USUARIO
 */
public interface UsuarioDao extends CrudRepository<Usuario, Integer> {

    @Transactional(readOnly=true)
    @Query(value="SELECT * FROM usuario WHERE activo = 1 and usuario =:usuario and clave =:clave", nativeQuery = true)
 
    public Usuario login(@Param("usuario") String usuario, @Param("clave") String clave);    

}
